<?php
require_once ("index.php");

class Frog extends Animal {
    public function jump(){
        echo "Jump: Hop Hop<br>";
    }

    public function __construct($name, $legs = 4, $cold_blooded = "no"){
        $this->name = $name;
        $this->legs = $legs;
        $this->cold_blooded = $cold_blooded;
    }
}

?>
<?php
require("index.php");
require("frog.php");
require("ape.php");

$sheep = new Animal("shaun");
echo $sheep->getName();
echo $sheep->getLegs();
echo $sheep->getBlood()."<br><br>";

$kodok = new Frog("buduk");
echo $kodok->getName();
echo $kodok->getLegs();
echo $kodok->getBlood();
$kodok->jump(); // "hop hop"
echo "<br><br>";

$sungokong = new Ape("kera sakti");
echo $sungokong->getName();
echo $sungokong->getLegs();
echo $sungokong->getBlood();
$sungokong->yell(); // "Auooo"
echo "<br>"; 


?>
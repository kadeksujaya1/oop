<?php
class Animal {
    public $legs,
           $cold_blooded,
           $name;
    
    public function __construct($name, $legs = 4, $cold_blooded = "no"){
        $this->name = $name;
        $this->legs = $legs;
        $this->cold_blooded = $cold_blooded;
    }

    public function getName(){
        return "Name: " . $this->name . "<br>";
    }

    public function getLegs(){
        return "legs: " . $this->legs . "<br>";
    }
    
    public function getBlood(){
        return "cold blooded: " . $this->cold_blooded . "<br>";
    }
}

?>